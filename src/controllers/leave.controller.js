
const LeaveModel = require('../models/leave.model');

// get all employee list
exports.getLeaveList = (req, res)=> {
    //console.log('here all employees list');
    LeaveModel.getAllLeave((err, employees) =>{
        console.log('Leaves are here');
        if(err)
        res.send(err);
        console.log('Leaves', employees);
        res.send(employees)
    })
}

// get employee by ID
exports.getLeaveByID = (req, res)=>{
    //console.log('get emp by id');
    LeaveModel.getEmployeeByID(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        console.log('single employee data',employee);
        res.send(employee);
    })
}

// create new employee
exports.createNewLeave = (req, res) =>{
    const employeeReqData = new LeaveModel(req.body);
    console.log('leaveReqData', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        LeaveModel.createLeave(employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Leave added Successfully', data: employee.insertId})
        })
    }
}

// update employee
exports.updateLeave = (req, res)=>{
    const employeeReqData = new LeaveModel(req.body);
    console.log('leaveReqData update', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        LeaveModel.updateLeave(req.params.id, employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'List updated Successfully'})
        })
    }
}

// delete employee
exports.deleteLeave = (req, res)=>{
    LeaveModel.deleteLeave(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'List deleted successully!'});
    })
}