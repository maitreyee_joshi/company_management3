
const AttendanceModel = require('../models/attendance_type.model');

// get all employee list
exports.getAttendanceList = (req, res)=> {
    //console.log('here all employees list');
    AttendanceModel.getAllAttendance((err, employees) =>{
        console.log('Attendance');
        if(err)
        res.send(err);
        console.log('Employees', employees);
        res.send(employees)
    })
}

// get employee by ID
exports.getAttendanceByID = (req, res)=>{
    //console.log('get emp by id');
    AttendanceModel.getAttendanceByID(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        console.log('single attendance data',employee);
        res.send(employee);
    })
}

// create new employee
exports.createNewAttendance = (req, res) =>{
    const employeeReqData = new AttendanceModel(req.body);
    console.log('attendanceReqData', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        AttendanceModel.createAttendance(employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Attendance Created Successfully', data: employee.insertId})
        })
    }
}

// update employee
exports.updateAttendance = (req, res)=>{
    const employeeReqData = new AttendanceModel(req.body);
    console.log('employeeReqData update', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        AttendanceModel.updateAttendance(req.params.id, employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'List updated Successfully'})
        })
    }
}

// delete employee
exports.deleteAttendance = (req, res)=>{
    EmployeeModel.deleteAttendance(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'Attendance deleted successully!'});
    })
}