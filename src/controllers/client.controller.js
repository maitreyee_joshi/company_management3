
const ClientModel = require('../models/client.model');

// get all employee list
exports.getClientList = (req, res)=> {
    //console.log('here all employees list');
    ClientModel.getAllClient((err, employees) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('Employees', employees);
        res.send(employees)
    })
}

// get employee by ID
exports.getClientByID = (req, res)=>{
    //console.log('get emp by id');
    ClientModel.getClientByID(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        console.log('single employee data',employee);
        res.send(employee);
    })
}

// create new employee
exports.createNewClient = (req, res) =>{
    const employeeReqData = new ClientModel(req.body);
    console.log('employeeReqData', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ClientModel.createClient(employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Employee Created Successfully', data: employee.insertId})
        })
    }
}

// update employee
exports.updateClient = (req, res)=>{
    const employeeReqData = new ClientModel(req.body);
    console.log('employeeReqData update', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ClientModel.updateClient(req.params.id, employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'List updated Successfully'})
        })
    }
}

// delete employee
exports.deleteClient = (req, res)=>{
    ClientModel.deleteClient(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'Client deleted successully!'});
    })
}