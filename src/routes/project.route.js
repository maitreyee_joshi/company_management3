const express = require('express');
const router = express.Router();

const projectsController = require('../controllers/project.controller');

// get all employees
router.get('/', projectsController.getProjectsList);

// get employee by ID
router.get('/:id',projectsController.getProjectsByID);

// create new employee
router.post('/', projectsController.createNewProjects);

// update employee
router.put('/:id', projectsController.updateProjects);

// delete employee
router.delete('/:id',projectsController.deleteProjects);

module.exports = router;