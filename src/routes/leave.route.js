const express = require('express');
const router = express.Router();

const leaveController = require('../controllers/leave.controller');

// get all employees
router.get('/', leaveController.getLeaveList);

// get employee by ID
router.get('/:id',leaveController.getLeaveByID);

// create new employee
router.post('/', leaveController.createNewLeave);

// update employee
router.put('/:id', leaveController.updateLeave);

// delete employee
router.delete('/:id',leaveController.deleteLeave);

module.exports = router;