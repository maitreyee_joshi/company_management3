const express = require('express');
const router = express.Router();

const attendanceController = require('../controllers/attendance_type.controller');

// get all employees
router.get('/', attendanceController.getAttendanceList);

// get employee by ID
router.get('/:id',attendanceController.getAttendanceByID);

// create new employee
router.post('/', attendanceController.createNewAttendance);

// update employee
router.put('/:id', attendanceController.updateAttendance);

// delete employee
router.delete('/:id',attendanceController.deleteAttendance);

module.exports = router;