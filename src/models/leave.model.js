var dbConn  = require('../../config/db.config');

var Leave = function(employee){
    this.leave_whosetaken          =   employee.leave_whosetaken;
    this.leave_fromdate            =   employee.leave_fromdate;
    this.leave_todate              =   employee.leave_todate;
    this.leave_status              =   employee.leave_status;
    this.leave_whose_approve       =   employee.leave_whose_approve;
    this.leave_rejection_desc      =   employee.leave_rejection_desc;
}

// get all employees
Leave.getAllLeave = (result) =>{
    dbConn.query('SELECT * FROM leave_table', (err, res)=>{
        if(err){
            console.log('Error while fetching data', err);
            result(null,err);
        }else{
            console.log('List fetched successfully');
            result(null,res);
        }
    })
}

// get employee by ID from DB
Leave.getLeaveByID = (id, result)=>{
    dbConn.query('SELECT * FROM leave_table WHERE lid=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching leaves by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new employee
Leave.createLeave = (employeeReqData, result) =>{
    dbConn.query('INSERT INTO leave_table SET ? ', employeeReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('List created successfully');
            result(null, res)
        }
    })
}

// update employee
Leave.updateLeave = (id, employeeReqData, result)=>{
    dbConn.query("UPDATE employees SET leave_whosetaken=?,leave_fromdate=?,leave_todate=?,leave_status=?,leave_whose_approve=?,leave_rejection_desc=? WHERE lid = ?", [employeeReqData.leave_whosetaken,employeeReqData.leave_fromdate,employeeReqData.leave_todate,leave_status,leave_whose_approve,leave_rejection_desc, lid], (err, res)=>{
        if(err){
            console.log('Error while updating the employee');
            result(null, err);
        }else{
            console.log("Employee updated successfully");
            result(null, res);
        }
    });
}

// delete employee
Leave.deleteLeave = (id, result)=>{
    dbConn.query('DELETE FROM leave_table WHERE lid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the entry');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = Leave;