var dbConn  = require('../../config/db.config');

var Attendance = function(employee){
    this.attendance_type_name   = employee.attendance_type_name;
}

// get all employees
Attendance.getAllAttendance = (result) =>{
    dbConn.query('SELECT * FROM attendance_type_table', (err, res)=>{
        if(err){
            console.log('Error while fetching employees', err);
            result(null,err);
        }else{
            console.log('employees fetched successfully');
            result(null,res);
        }
    })
}

// get employee by ID from DB
Attendance.getAttendanceByID = (id, result)=>{
    dbConn.query('SELECT * FROM attendance_type_table WHERE aid=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching attendance by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new employee
Attendance.createAttendance = (employeeReqData, result) =>{
    dbConn.query('INSERT INTO attendance_type_table SET ? ', employeeReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('attendance created successfully');
            result(null, res)
        }
    })
}

// update employee
Attendance.updateAttendance = (id, employeeReqData, result)=>{
    dbConn.query("UPDATE attendance_type_name=? WHERE aid = ?", [employeeReqData.attendance_type_name, aid], (err, res)=>{
        if(err){
            console.log('Error while updating the attendance');
            result(null, err);
        }else{
            console.log("Attendance updated successfully");
            result(null, res);
        }
    });
}

// delete employee
Attendance.deleteAttendance = (id, result)=>{
    dbConn.query('DELETE FROM attendance_type_table WHERE aid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the attendance');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = Attendance;